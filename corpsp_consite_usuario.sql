USE [SEGCORP]
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[corpsp_consite_usuario]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[corpsp_consite_usuario]
GO


SET ANSI_NULLS ON
GO


SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[corpsp_consite_usuario]
(
	@cd_perfil int,
	@cd_retorno int output,
	@nm_retorno varchar(200) output,
	@id_usuario int = null,
	@cd_usuario VARCHAR(10) = NULL, -- Usu�rio da sess�o
	@nr_versao_controle	varchar(20) = null  output,
	@dt_versao_controle	varchar(50)	= null  output
)

AS

------ VERS�O DA PROCEDURE ---------------------------------------
SELECT @nr_versao_controle = '$Revision: 1.2 $',
	   @dt_versao_controle = '$Date: 2018/07/05 15:24:03 $'
-------------------------------------------------------------------



SELECT @nr_versao_controle = RTRIM(LTRIM(REPLACE(REPLACE(@nr_versao_controle,'Revision:',''),'$',''))),
	   @dt_versao_controle = RTRIM(LTRIM(REPLACE(REPLACE(@dt_versao_controle,'Date:',''),'$','')))




set @cd_retorno = 0
set @nm_retorno = ''

declare @interno bit

select @interno = isnull(dv_acesso_interno, 0)
from dina_perfil
where cd_perfil = @cd_perfil

if(@interno = 1) and dbo.corpfc_permissao_gia(@id_usuario, @cd_usuario) = 0
begin
	set @cd_retorno = 1
	set @nm_retorno = 'Perfis do tipo Interno s� ser�o associados aos usu�rios pelo sistema GIA.'
	return
end

return