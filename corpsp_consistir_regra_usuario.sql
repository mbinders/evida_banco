USE [SEGCORP]
GO

/****** Object:  StoredProcedure [dbo].[corpsp_consistir_regra_usuario]    Script Date: 01/14/2016 14:53:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[corpsp_consistir_regra_usuario]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[corpsp_consistir_regra_usuario]
GO

USE [SEGCORP]
GO

/****** Object:  StoredProcedure [dbo].[corpsp_consistir_regra_usuario]    Script Date: 01/14/2016 14:53:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE dbo.corpsp_consistir_regra_usuario
(
	@Cd_usuario_detalhe varchar(10),
	@cd_regra int,
	@dv_regra bit,
	@DataAction varchar(50),
	@vl_regra text,
	@cd_retorno int output,
	@nm_retorno varchar(200) output
)
as

--$Id: corpsp_consistir_regra_usuario.sql,v 1.3 2019/07/30 13:17:09 marcelo Exp $

set @cd_retorno = 0
set @nm_retorno = ''

declare @interno bit
set @interno = 0

select @interno = perfil.dv_acesso_interno
from dina_usuario usuario
inner join dina_perfil perfil
	on usuario.cd_perfil = perfil.cd_perfil
where cd_usuario = @Cd_usuario_detalhe

if(@DataAction = 'inserir' and @interno = 0 and @cd_regra in (8)) --regras apenas para usuarios internos
begin
	set @cd_retorno = 1
	set @nm_retorno = 'N�o pode adicionar esta regra para usuarios EXTERNOS.'
	return
end

if (@DataAction in ('alterar', 'excluir', 'inserir'))
begin

	-- S� houve altera��o do campo dv_regra. REQ 57 - "A �nica exce��o � que o sistema dever� permitir marcar uma regra como Ativa ou n�o (Flag Ativo?)"
	if (@DataAction = 'alterar' and exists (
											select 1
											  from corp_usuario_regra cur
											  where cur.Cd_usuario = @Cd_usuario_detalhe
												and cur.cd_regra = @cd_regra
												and cur.dv_regra <> @dv_regra
												and isnull(cur.vl_regra, '') like isnull(@vl_regra, '')))
	begin
		return
	end 

	declare @id_pacote_regra int

	set @id_pacote_regra = null

	select @id_pacote_regra = id_pacote_regra
	from dina_usuario usuario
		inner join dina_perfil perfil
			on usuario.Cd_perfil = perfil.cd_perfil
	where usuario.cd_usuario = @Cd_usuario_detalhe

	if(@id_pacote_regra is not null)
	begin
		set @cd_retorno = 1
		set @nm_retorno = 'N�o � poss�vel alterar as regras diretamente no cadastro do usu�rio. Favor aplicar a altera��o no pacote de regras associado ao perfil desse usu�rio.'
		return
	end
end

return
GO