USE [SEGCORP]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[corpsp_cons_ise_lotes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[corpsp_cons_ise_lotes]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[corpsp_cons_ise_lotes] (
	@orderby		varchar(8000)
	,@where			varchar(8000)
	,@cd_retorno			int				= 0		outPut
	,@nm_retorno			varchar(8000)	= null	outPut
	,@nr_versao_controle	varchar(20)		= null	output
	,@dt_versao_controle	varchar(50)		= null	output
)
as
BEGIN

------ VERS�O DA PROCEDURE ---------------------------------------
SELECT @nr_versao_controle = '$Revision: 1.3 $',
	   @dt_versao_controle = '$Date: 2020/07/23 19:48:51 $'
-------------------------------------------------------------------

SELECT @nr_versao_controle = RTRIM(LTRIM(REPLACE(REPLACE(@nr_versao_controle,'Revision:',''),'$',''))),
	   @dt_versao_controle = RTRIM(LTRIM(REPLACE(REPLACE(@dt_versao_controle,'Date:',''),'$','')))




	SET noCount ON
	declare @sql		varchar(max)
			,@cd_lote	int
			,@nr_grupo	int



	Select	@cd_lote	= convert(int, dbo.corpfc_extrai_valor_where(@where, 'ciliip.cd_lote'))
			,@nr_grupo	= convert(int, dbo.corpfc_extrai_valor_where(@where, 'ciliip.nr_grupo'))

select @sql = 
'select		ciliip.cd_lote
		   ,ciliip.nr_grupo
		   ,qtd_propostas = COUNT(id_importacao)
		   ,qdt_propostas_aceitas = prop_ac.qtd_propostas_ac
		   ,qdt_propostas_recusadas = COUNT(id_importacao) - prop_ac.qtd_propostas_ac
	from corp_interface_lote_ise_individual_proposta ciliip
	outer apply (select top 1 qtd_propostas_ac = COUNT(ca1.id_apolice)
				 from corp_apolice ca1
				 where ca1.Id_apolice = ciliip.id_apolice
				 and ca1.cd_status in (7,9)) prop_ac
	where ciliip.dv_processado = 1 '+
	case when @cd_lote is not null and @nr_grupo is null then 'and ciliip.cd_lote = '+convert(varchar,@cd_lote)+' '
				when @cd_lote is null and @nr_grupo is not null then 'and ciliip.nr_grupo = '+convert(varchar,@nr_grupo) +' '
				when @cd_lote is not null and @nr_grupo is not null then 'and ciliip.cd_lote = '+convert(varchar,@cd_lote) +'and ciliip.nr_grupo='+convert(varchar,@nr_grupo)+' '
					else ' ' end +
	'group by ciliip.cd_lote
		   ,ciliip.nr_grupo
		   ,prop_ac.qtd_propostas_ac' +
		   
		   
		   +@orderby

	exec(@sql)	   

	select @cd_retorno = 0
			,@nm_retorno = ''
end