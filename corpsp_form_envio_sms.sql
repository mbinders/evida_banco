use SEGCORP
go


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[corpsp_form_envio_sms]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[corpsp_form_envio_sms]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[corpsp_form_envio_sms]
(
 @dt_inicio					smalldatetime	= NULL,
 @dt_fim					smalldatetime	= NULL,
 @cd_retorno				INT				= NULL  OUTPUT,
 @nm_retorno				VARCHAR(200)	= NULL  OUTPUT,
 @nr_versao_controle		VARCHAR(20)		= NULL  OUTPUT,
 @dt_versao_controle		VARCHAR(50)		= NULL  OUTPUT


  
)
AS  
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	Set Ansi_Warnings Off


	/*

	Declare @cd_retorno int
	Declare @nm_retorno varchar(255)
	
	
	exec corpsp_form_envio_sms  @dt_inicio = '20210201', @dt_periodo_fim = '20210225', @cd_retorno = @cd_retorno output, @nm_retorno = @nm_retorno output

	Print @cd_retorno
	Print @nm_retorno


	*/



	------ VERS�O DA PROCEDURE ---------------------------------------
	SELECT @nr_versao_controle = '$Revision: 1.2 $',
		   @dt_versao_controle = '$Date: 2021/08/11 20:52:40 $'
	------------------------------------------------------------------


	SELECT @nr_versao_controle = RTRIM(LTRIM(REPLACE(REPLACE(@nr_versao_controle,'Revision:',''),'$',''))),
		   @dt_versao_controle = RTRIM(LTRIM(REPLACE(REPLACE(@dt_versao_controle,'Date:',''),'$','')))


select cpm.id_parcela_movimentacao, cep.id_parcela, cep.id_endosso, cfp.nr_banco, cfp.nm_forma_pagamento
into #parcelas
from corp_parc_movto cpm
join corp_endosso_parcela cep
on cep.id_parcela = cpm.id_parcela
and cep.dv_situacao = 'P'
and isnull(cep.dt_ultimo_vencimento,cpm.dt_vencimento) = cpm.dt_vencimento
join corp_forma_pagamento cfp
on cfp.Cd_forma_pagamento = cpm.Cd_forma_pagamento
and cfp.Cd_forma_pagamento <> 998
where cpm.dt_vencimento between @dt_inicio and @dt_fim
and cpm.cd_evento in (101,601)


select	cap.nr_grupo, 
		cap.cd_apolice, 
		csep.nr_sub, 
		ce.cd_proposta, 
		cp.nm_pessoa, 
		dt_competencia = convert(varchar,ce.dt_competencia,103),
		cpm.vl_total, 
		dt_vencimento = convert(varchar,cpm.dt_vencimento,103),
		p.nm_forma_pagamento, 
		p.nr_banco, 
		cpm.nr_agencia, 
		cpm.nr_dac_agencia, 
		cpm.nr_conta_corrente, 
		cpm.nr_dac_conta_corrente, 
		ddd_celular_pessoa = cp.nr_ddd_celular, 
		celular_pessoa = cp.nr_celular, 
		ddd_fone_endereco = cend.nr_ddd, 
		fone_endereco = cend.nr_fone,
		cp.nm_email, 
		cd_tipo_pessoa = case when cp.cd_tipo_pessoa = 1 then 'PF' else 'PJ' end, 
		rb.nm_ocorrencia, 
		rb.nm_motivo
from #parcelas p
join corp_parc_movto cpm
on cpm.id_parcela_movimentacao = p.id_parcela_movimentacao
join corp_endosso_parcela cep
on cep.id_parcela = cpm.id_parcela
join corp_endosso ce
on ce.id_endosso = cep.id_endosso
and ce.cd_tipo_endosso < 5
and ISNULL(ce.dv_estornado,0) = 0
join corp_sub_estipulante cse
on cse.id_sub = ce.id_sub
join corp_apolice ca
on ca.id_apolice = cse.id_apolice
and ca.cd_status = 7
join corp_sub_estipulante csep
on csep.id_sub = isnull(ca.id_sub_pai,cse.id_sub)
join corp_apolice cap
on cap.id_apolice = csep.id_apolice
and cap.cd_status = 7
join corp_pessoas cp
on cp.id_pessoa = cse.id_pessoa_sub
left join corp_endereco cend
on cend.id_endereco = ce.id_endereco
left join corp_sub_regra csr28
on csr28.id_sub = csep.id_sub
and csr28.cd_regra = 28
left join corp_sub_regra csr93
on csr93.id_sub = csep.id_sub
and csr93.cd_regra = 93
outer apply (
 select top 1
     nm_ocorrencia = right('000'+cco.cd_ocorrencia,12) + ' - ' + cco.nm_ocorrencia,  
     nm_motivo = case when (isnull(crcd.nm_motivo,'') <> '' or isnumeric(crcd.cd_ocorrencia) = 0)  
          then null  
          else ( select top 1 '<B>' + cbmo.nm_motivo + ': '+'</B>' + cbmo.nm_erro  
           from corp_banco_motivo_ocorrencia cbmo  
           where cbmo.nr_banco = cfp.nr_banco  
            and cbmo.cd_ocorrencia = crcd.Cd_ocorrencia  
            and cbmo.cd_motivo = substring(crcd.ds_linha,
					convert(int,(select convert(varchar(100),substring(cbmo2.nm_erro,1,charindex(',',cbmo2.nm_erro)-1)) from corp_banco_motivo_ocorrencia cbmo2 where cbmo2.nr_banco = cbmo.nr_banco and cbmo2.cd_ocorrencia = cbmo.cd_ocorrencia and cbmo2.nm_motivo = '*** POSICAO/QTD POSICOES ***')),
					convert(int,(select convert(varchar(100), substring(cbmo2.nm_erro,charindex(',',cbmo2.nm_erro)+1,100)) from corp_banco_motivo_ocorrencia cbmo2 where cbmo2.nr_banco = cbmo.nr_banco and cbmo2.cd_ocorrencia = cbmo.cd_ocorrencia and cbmo2.nm_motivo = '*** POSICAO/QTD POSICOES ***')))  
           )  
         end
	 from corp_retorno_cobranca_detalhe crcd
     join corp_retorno_cobranca_header crch  
		on (crcd.cd_importacao = crch.cd_importacao  
	   and crcd.cd_forma_pagamento = crch.cd_forma_pagamento)  
     join corp_forma_pagamento cfp  
		on cfp.cd_forma_pagamento = crcd.cd_forma_pagamento  
     left join corp_campo_ocorrencia cco  
		on (cco.id_campo = crcd.id_campo  
       and cco.cd_ocorrencia = crcd.cd_ocorrencia)  
	 where crcd.nr_cobranca= cpm.nr_cobranca 
	 order by crch.dt_geracao desc
) rb
where (ca.cd_tp_apolice = 4 and (isnull(csr28.dv_regra,0) = 0 or isnull(csr93.dv_regra,0) = 0))
or (ca.cd_tp_apolice = 2 and isnull(csr28.dv_regra,0) = 1 and isnull(csr93.dv_regra,0) = 0)
order by 8, 5


if object_id('tempdb..#parcelas') is not null
	Begin
	  Drop table #parcelas
	End

end

